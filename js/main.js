$(document).ready(function(){
    // Check login status
    function checkLoggedInState(){
        let loggedInUser = localStorage.getItem("user");
        console.log(loggedInUser)
        if(loggedInUser) {
            $("#post-main").css({"height":"1075vh"});
            $("#post-2-main").css({"height":"195vh"});
            $(".user-auth").hide();
            $(".login").show();
            $(".main-row-1-buttons .btn-success").show();
            $(".anonym-space-link").hide();
            $(".forum-channels").show();
            $(".editor").show();
            $(".bottom-registration").hide();
            addOrRemoveLikes();

            
            var user = JSON.parse(loggedInUser);
            $("#user-name").text(user.fName);
            $(".user-acc-logo").text(user.fName[0]);
            $(".user-avatar > div").text(user.fName[0]);
            $(".am-1-left").text(user.fName[0]);
            $(".am-1-right a").text(user.fName);

        } else {
            $(".user-auth").show();
            $(".login").hide();
            $(".main-row-1-buttons .btn-success").hide();
            $(".anonym-space-link").show();
            $(".forum-channels").hide();
            $(".editor").hide();
            $("#post-main").css({"height":"1042vh"});
            $("#post-2-main").css({"height":"162vh"});
            $("#user-name").text("");
            $(".bottom-registration").show();
            removeLikeAfterLogout();
            openLoginSecondary();
        }
    }

    checkLoggedInState();

    // Register user
    $(document).on("click", "#register-btn", function(e){
        e.preventDefault();

        var fName = $('.registration-modal .fname').val(); 
        var email = $('.registration-modal .email').val(); 
        var password = $('.registration-modal .password').val();

        var user = {
            fName: fName,
            email: email,
            password: password,
        }

        var registeredUsers = localStorage.getItem("registeredUsers");

        if(!registeredUsers){
           localStorage.setItem("registeredUsers", JSON.stringify([]));
           registeredUsers = [];
        }
        else {
            registeredUsers = JSON.parse(registeredUsers);
        }

        registeredUsers.push(user);
        localStorage.setItem("registeredUsers", JSON.stringify(registeredUsers));
        $(".registration-modal").remove();

        localStorage.setItem("user", JSON.stringify(user));
        checkLoggedInState();
    });

    // Login user
    $(document).on("click", "#login-btn", function(e){
        e.preventDefault();

        var fName = $('.login-window .fname').val();
        var password = $('.login-window .password').val();

        var user = {
            fName: fName,
            password: password,
        }

        var registeredUsers = localStorage.getItem("registeredUsers");
        
        if(registeredUsers){
            registeredUsers = JSON.parse(registeredUsers);

            var currentUser = registeredUsers.filter(function(item){
                return item.fName == fName && item.password == password;
            });

            if(currentUser.length > 0){       
                localStorage.setItem("user", JSON.stringify(user));
                $(".login-window").remove();
                checkLoggedInState();
            }
        }
    });

    // Logout user
    $(document).on("click", "#logout", function(){
        localStorage.removeItem("user");
        checkLoggedInState();
    });

    $("#user-name").on("click", function(){
        showUserAcc();
    });

    $(document).on("click", function(e) {
        if(!$(e.target).is('#user-name, #user-name *')){
            hideUserAcc();
        }
    });

    $(".user-acc").on("click", function(e) {
        if(!$(e.target).is('#logout, #logout *')) {
            e.stopPropagation(); 
        }      
    });

    // Deletes shov class from Bootstrap navbar
    $(".fa-times").click(function(){
        $( ".navbar-collapse.show" ).removeClass( "show" );
    });

    // Shows loading animation before opening registration form
    $(".registration").click(function(){
        $( ".lds-spinner" ).fadeIn().delay(20).fadeOut();
        let openModal = setTimeout(openRegModal, 1000);
    });
    
    // Open login modal window
    $(".sign-in").click(function(){
        $( ".lds-spinner" ).fadeIn().delay(20).fadeOut();
        let openModal = setTimeout(openLoginModal, 1000);
    });

    // Closes login modal window
    $(document).on("click", ".login-window .fa-times", function(){
        $(".login-window").fadeOut();
    });

    // Closes registration modal window
    $(document).on("click", ".registration-modal .fa-times", function(){
        $(".registration-modal").fadeOut();
    });

    // Opens login window from comments bottom section
    function openLoginSecondary() {
        $(".bottom-registration").click(function(){
            $( ".lds-spinner" ).fadeIn().delay(20).fadeOut();
            let openModal = setTimeout(openLoginModal, 1000);
        });
    }    

    function addOrRemoveLikes() {
        $(document).on("click", ".po-like i", function(){
            if($(".po-like i").hasClass("liked")) {
                $(".po-like i").removeClass("liked");
                // let likeNum = document.getElementById("like-num").innerHTML;
                document.getElementById("like-num").innerHTML = "200";
            } else if (!$(".po-like i").hasClass("liked")) {
                $(".po-like i").addClass("liked");
                document.getElementById("like-num").innerHTML = "201";
            }
        });

        $(document).on("click", ".like-1 i", function(){
            if($(".like-1 i").hasClass("liked")) {
                $(".like-1 i").removeClass("liked");
                document.getElementById("like-num-1").innerHTML = "50";
            } else if (!$(".like-1 i").hasClass("liked")) {
                $(".like-1 i").addClass("liked");
                document.getElementById("like-num-1").innerHTML = "51";
            }
        });
    
        $(document).on("click", ".like-2 i", function(){
            if($(".like-2 i").hasClass("liked")) {
                $(".like-2 i").removeClass("liked");
                document.getElementById("like-num-2").innerHTML = "50";
            } else if (!$(".like-2 i").hasClass("liked")) {
                $(".like-2 i").addClass("liked");
                document.getElementById("like-num-2").innerHTML = "51";
            }
        });
    
        $(document).on("click", ".like-3 i", function(){
            if($(".like-3 i").hasClass("liked")) {
                $(".like-3 i").removeClass("liked");
                document.getElementById("like-num-3").innerHTML = "50";
            } else if (!$(".like-3 i").hasClass("liked")) {
                $(".like-3 i").addClass("liked");
                document.getElementById("like-num-3").innerHTML = "51";
            }
        });
    
        $(document).on("click", ".like-4 i", function(){
            if($(".like-4 i").hasClass("liked")) {
                $(".like-4 i").removeClass("liked");
                document.getElementById("like-num-4").innerHTML = "50";
            } else if (!$(".like-4 i").hasClass("liked")) {
                $(".like-4 i").addClass("liked");
                document.getElementById("like-num-4").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-5 i", function(){
            if($(".like-5 i").hasClass("liked")) {
                $(".like-5 i").removeClass("liked");
                document.getElementById("like-num-5").innerHTML = "50";
            } else if (!$(".like-5 i").hasClass("liked")) {
                $(".like-5 i").addClass("liked");
                document.getElementById("like-num-5").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-6 i", function(){
            if($(".like-6 i").hasClass("liked")) {
                $(".like-6 i").removeClass("liked");
                document.getElementById("like-num-6").innerHTML = "50";
            } else if (!$(".like-6 i").hasClass("liked")) {
                $(".like-6 i").addClass("liked");
                document.getElementById("like-num-6").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-7 i", function(){
            if($(".like-7 i").hasClass("liked")) {
                $(".like-7 i").removeClass("liked");
                document.getElementById("like-num-7").innerHTML = "50";
            } else if (!$(".like-7 i").hasClass("liked")) {
                $(".like-7 i").addClass("liked");
                document.getElementById("like-num-7").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-8 i", function(){
            if($(".like-8 i").hasClass("liked")) {
                $(".like-8 i").removeClass("liked");
                document.getElementById("like-num-8").innerHTML = "50";
            } else if (!$(".like-8 i").hasClass("liked")) {
                $(".like-8 i").addClass("liked");
                document.getElementById("like-num-8").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-9 i", function(){
            if($(".like-9 i").hasClass("liked")) {
                $(".like-9 i").removeClass("liked");
                document.getElementById("like-num-9").innerHTML = "50";
            } else if (!$(".like-9 i").hasClass("liked")) {
                $(".like-9 i").addClass("liked");
                document.getElementById("like-num-9").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-10 i", function(){
            if($(".like-10 i").hasClass("liked")) {
                $(".like-10 i").removeClass("liked");
                document.getElementById("like-num-10").innerHTML = "50";
            } else if (!$(".like-10 i").hasClass("liked")) {
                $(".like-10 i").addClass("liked");
                document.getElementById("like-num-10").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-11 i", function(){
            if($(".like-11 i").hasClass("liked")) {
                $(".like-11 i").removeClass("liked");
                document.getElementById("like-num-11").innerHTML = "50";
            } else if (!$(".like-11 i").hasClass("liked")) {
                $(".like-11 i").addClass("liked");
                document.getElementById("like-num-11").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-12 i", function(){
            if($(".like-12 i").hasClass("liked")) {
                $(".like-12 i").removeClass("liked");
                document.getElementById("like-num-12").innerHTML = "50";
            } else if (!$(".like-12 i").hasClass("liked")) {
                $(".like-12 i").addClass("liked");
                document.getElementById("like-num-12").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-13 i", function(){
            if($(".like-13 i").hasClass("liked")) {
                $(".like-13 i").removeClass("liked");
                document.getElementById("like-num-13").innerHTML = "50";
            } else if (!$(".like-13 i").hasClass("liked")) {
                $(".like-13 i").addClass("liked");
                document.getElementById("like-num-13").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-14 i", function(){
            if($(".like-14 i").hasClass("liked")) {
                $(".like-14 i").removeClass("liked");
                document.getElementById("like-num-14").innerHTML = "50";
            } else if (!$(".like-14 i").hasClass("liked")) {
                $(".like-14 i").addClass("liked");
                document.getElementById("like-num-14").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-15 i", function(){
            if($(".like-15 i").hasClass("liked")) {
                $(".like-15 i").removeClass("liked");
                document.getElementById("like-num-15").innerHTML = "50";
            } else if (!$(".like-15 i").hasClass("liked")) {
                $(".like-15 i").addClass("liked");
                document.getElementById("like-num-15").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-16 i", function(){
            if($(".like-16 i").hasClass("liked")) {
                $(".like-16 i").removeClass("liked");
                document.getElementById("like-num-16").innerHTML = "50";
            } else if (!$(".like-16 i").hasClass("liked")) {
                $(".like-16 i").addClass("liked");
                document.getElementById("like-num-16").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-17 i", function(){
            if($(".like-17 i").hasClass("liked")) {
                $(".like-17 i").removeClass("liked");
                document.getElementById("like-num-17").innerHTML = "50";
            } else if (!$(".like-17 i").hasClass("liked")) {
                $(".like-17 i").addClass("liked");
                document.getElementById("like-num-17").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-18 i", function(){
            if($(".like-18 i").hasClass("liked")) {
                $(".like-18 i").removeClass("liked");
                document.getElementById("like-num-18").innerHTML = "50";
            } else if (!$(".like-18 i").hasClass("liked")) {
                $(".like-18 i").addClass("liked");
                document.getElementById("like-num-18").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-19 i", function(){
            if($(".like-19 i").hasClass("liked")) {
                $(".like-19 i").removeClass("liked");
                document.getElementById("like-num-19").innerHTML = "50";
            } else if (!$(".like-19 i").hasClass("liked")) {
                $(".like-19 i").addClass("liked");
                document.getElementById("like-num-19").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-20 i", function(){
            if($(".like-20 i").hasClass("liked")) {
                $(".like-20 i").removeClass("liked");
                document.getElementById("like-num-20").innerHTML = "50";
            } else if (!$(".like-20 i").hasClass("liked")) {
                $(".like-20 i").addClass("liked");
                document.getElementById("like-num-20").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-21 i", function(){
            if($(".like-21 i").hasClass("liked")) {
                $(".like-21 i").removeClass("liked");
                document.getElementById("like-num-21").innerHTML = "50";
            } else if (!$(".like-21 i").hasClass("liked")) {
                $(".like-21 i").addClass("liked");
                document.getElementById("like-num-21").innerHTML = "51";
            }
        });

        $(document).on("click", ".like-22 i", function(){
            if($(".like-22 i").hasClass("liked")) {
                $(".like-22 i").removeClass("liked");
                document.getElementById("like-num-22").innerHTML = "50";
            } else if (!$(".like-22 i").hasClass("liked")) {
                $(".like-22 i").addClass("liked");
                document.getElementById("like-num-22").innerHTML = "51";
            }
        });
    }

    function removeLikeAfterLogout() {
        if($(".likes i").hasClass("liked")) {
            $(".likes i").removeClass("liked");
        }
    }
});

// Appends the reg-form
function openRegModal() {
    $( "body" ).append( 
        `<form class="registration-modal">
            <div class="reg-title">
                <span>Регистрация</span>
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <div class="mod">
                <div class="mod-col-1">
                    <div class="col-1-row-1">
                        <span id="fname">Имя пользователя:</span>
                        <span>Обязательно</span>
                    </div>
                    <div class="col-1-row-2">
                        <span id="email">Электронная почта:</span>
                        <span>Обязательно</span>
                    </div>
                    <div class="col-1-row-3">
                        <span id="password" >Пароль:</span>
                        <span>Обязательно</span>

                    </div>
                    <div class="col-1-row-4">
                        <span id="telegram">Telegram:</span>
                    </div>
                    <div class="col-1-row-5">
                        <span>Проверка:</span>
                        <span>Обязательно</span>
                    </div>
                </div>

                <div class="mod-col-2">
                    <div class="col-2-row-1">
                        <input type="text" id="fname" name="fname" class="fname">
                        <p>Это имя будет отображаться в Ваших сообщениях. Можно использовать любое имя. 
                            Его нельзя будет поменять после регистрации.
                        </p>
                    </div>
                    <div class="col-2-row-2">
                        <input type="text" id="email" name="email" class="email">
                    </div>
                    <div class="col-2-row-3">
                        <input type="password" id="password" name="password" class="password">
                        <p>Необходимо указать пароль.</p>
                    </div>
                    <div class="col-2-row-4">
                        <input type="text" id="telegram" name="telegram" class="telegram">
                        <p>(НЕ обязательное поле) Ваш логин телеграм (без @).</p>
                    </div>
                    <div class="col-2-row-5">
                        <div class="g-recaptcha brochure__form__captcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                    </div>
                    <div class="col-2-row-6">
                        <input type="checkbox" id="terms" name="terms">
                        <p>Я согласен(на) с условиями и политикой конфиденциальности.</p>
                    </div>
                </div>
            </div>
            <div class="mod-row-1">
                <button class="btn btn-success text-white disabled" id="register-btn" type="button" >Регистрация</button> 
            </div>
        </form>` );
    // $( "body" ).css( "opacity",".8" );
    // $( "registration-modal" ).css( "opacity","1" );
}

// Appends the login form
function openLoginModal() {
    $( "body" ).prepend( 
        `
        <div class="login-window">
        <div class="lw-title">
            <span>Вход</span>
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>

        <div class="lw-main">
            <div class="lw-main-left">
                <div class="main-l-row">
                    <span>Имя пользователя или email:</span>
                    <span>Пароль:</span>
                </div>
            </div>

            <div class="lw-main-right">
                <form class="login-input">
                    <div class="li-row">
                        <div class="li-row-1">
                            <input type="text" id="fname" name="fname" class="fname">
                        </div>
    
                        <div class="li-row-2">
                            <input type="password" id="password" name="password" class="password">
                            <a href="javascript:void();">Забыли свой пароль?</a>
                        </div>
                    </div>

                    <div class="lm-row-3">
                        <input type="checkbox" id="fname" name="fname" class="fname">
                        <span>Запомнить меня</span>
                    </div>
                </form>
            </div>
        </div>

        <div class="lw-footer">
            <button class="btn btn-success" id="login-btn">
                <i class="fa fa-lock" aria-hidden="true"></i>Войти
            </button>

            <div class="lw-footer-reg">
                <p>У Вас ещё нет учётной записи?</p>
                <div>Зарегистрируйтесь</div>
            </div>
        </div>
    </div>
        ` );
}

function hideUserAcc() {
    $('.user-acc').css("display", "none");
}

function showUserAcc() {
    $('.user-acc').css("display", "block");
}

// Removes disabled class from reg-form if terms checkbox is checked
document.addEventListener('click', function(e) { 
    $(document).ready(function(){
        if($('input[id="terms"]').is(':checked')) {
            $( ".mod-row-1 > .btn.disabled" ).removeClass( "disabled" );
            $( ".mod-row-1 > .btn-success.disabled" ).removeClass( "disabled" );
        } else {
            $( ".mod-row-1 > .btn" ).addClass( "disabled" );
        }
    });
});

// Add modal window for post owner info on post owner nick mouseover
document.addEventListener('mouseover', function(e) { 
    $(".po-avatar-row-1 a").mouseover(function(){
        $(".post-avatar").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo">
                        <img src="../images/Robert Lutece avatar.jpg" alt="R. Lutect" id="po-modal-img">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">Robert Lutece</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>24 Окт 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>4 Ноя 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p class="counter">15000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>12д 14ч 52м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p class="counter">150</p>
                    </div>


                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>175</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>425</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
        // $(".owner-info-modal").css({"top":"-15%", "left":"-19%"});
    });
});
$(".po-avatar-row-1 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});


// Add modal window on post owner image mouseover
document.addEventListener('mouseover', function(e) { 
    $(".po-avatar-row-2 img").mouseover(function(){
        $(".po-avatar-row-2").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo">
                        <img src="../images/Robert Lutece avatar.jpg" alt="R. Lutect" id="po-modal-img">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">Robert Lutece</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>24 Окт 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>4 Ноя 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>15000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>12д 14ч 52м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>150</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>175</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>425</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
        // $(".owner-info-modal").css({"top":"-15%", "left":"-19%"});
    });
});
$(".po-avatar-row-2").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});


// Add modal window for comment owner info
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-1 a").mouseover(function(){
        $(".com-avatar-1").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
        // $(".owner-info-modal").css({"top":"-15%", "left":"-19%"});
    });
});
$(".co-nick-1 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Comment owner -2
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-2 a").mouseover(function(){
        $(".com-avatar-2").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-2 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});


// Comment owner - 3
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-3 a").mouseover(function(){
        $(".com-avatar-3").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-3 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 4
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-4 a").mouseover(function(){
        $(".com-avatar-4").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-4 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 5
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-5 a").mouseover(function(){
        $(".com-avatar-5").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-5 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 6
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-6 a").mouseover(function(){
        $(".com-avatar-6").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-6 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 7
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-7 a").mouseover(function(){
        $(".com-avatar-7").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-7 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 8
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-8 a").mouseover(function(){
        $(".com-avatar-8").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-8 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 9
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-9 a").mouseover(function(){
        $(".com-avatar-9").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-9 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner - 10
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-10 a").mouseover(function(){
        $(".com-avatar-10").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-10 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});


// Com owner 11
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-11 a").mouseover(function(){
        $(".com-avatar-11").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-11 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 12
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-12 a").mouseover(function(){
        $(".com-avatar-12").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-12 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 13
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-13 a").mouseover(function(){
        $(".com-avatar-13").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-13 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 14
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-14 a").mouseover(function(){
        $(".com-avatar-14").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-14 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 15
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-15 a").mouseover(function(){
        $(".com-avatar-15").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-15 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 16
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-16 a").mouseover(function(){
        $(".com-avatar-16").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-16 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 17
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-17 a").mouseover(function(){
        $(".com-avatar-17").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-17 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 18
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-18 a").mouseover(function(){
        $(".com-avatar-18").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-18 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 19
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-19 a").mouseover(function(){
        $(".com-avatar-19").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-19 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});

// Com owner 20
document.addEventListener('mouseover', function(e) { 
    $(".co-nick-20 a").mouseover(function(){
        $(".com-avatar-20").prepend(`
        <div class="owner-info-modal">
            <div class="oi-main">
                <div class="oi-main-row-1">
                    <div class="user-logo user-logo-modal">
                        <img src="../images/IvanDV92.jpg" alt="IvanDV92">
                    </div>

                    <div class="user-details">
                        <a href="javascript:void();">IvanDV92</a>

                        <div class="ud-row-1">
                            <div class="progress-start">
                                <span>МЕСТНЫЙ</span>
                            </div>

                            <div class="progress-end">
                                <span>VLMI.IO</span>
                            </div>
                        </div>

                        <div class="ud-row-2">
                            <div class="reg-date">
                                <span>Регистрация:</span>
                                <p>15 Апр 2017</p>
                            </div>

                            <div class="activity">
                                <span>Активность:</span>
                                <p>20 Дек 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="oi-main-row-2">
                    <div class="user-stat-1">
                        <span>Депозит</span>
                        <p>1000</p>
                    </div>

                    <div class="user-stat-2">
                        <span>Времени онлайн</span>
                        <p>8д 1ч 42м</p>
                    </div>

                    <div class="user-stat-3">
                        <span>Сообщения</span>
                        <p>100</p>
                    </div>

                    <div class="user-stat-4">
                        <span>Реакции</span>
                        <p>105</p>
                    </div>

                    <div class="user-stat-5">
                        <span>Баллы</span>
                        <p>125</p>
                    </div>
                </div>
            </div>

            <div class="oi-triangle">
                <div class="triangle"></div>
            </div>
        </div>
        `);
    });
});
$(".com-avatar-20 a").mouseleave(function() {
    $(".owner-info-modal").delay(1000).fadeOut();
});


// Smooth scrolling
$(document).ready(function() {
    'use strict';
    
    $('.scroll-to-top a, .scroll-to-bottom a').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
  });    
});

// Go to top/bottom
$(".scroll").fadeOut();
let timer;
window.onscroll = function(e) {
    clearTimeout(timer);
    // print "false" if direction is down and "true" if up
    if(this.oldScroll > this.scrollY) {
        // console.log("up");
        $(".scroll").fadeIn();
        $(".scroll-to-top").css("font-size", "16px");
        $(".scroll-to-bottom").css("font-size", "12px");
        timer =setTimeout(function () {
            $(".scroll").fadeOut();
        }, 2000);
    } else if (this.oldScroll <= this.scrollY) {
        $(".scroll").fadeIn();
        $(".scroll-to-top").css("font-size", "12px");
        $(".scroll-to-bottom").css("font-size", "16px");
        timer = setTimeout(function () {
            $(".scroll").fadeOut();
        }, 2000);
    }

    this.oldScroll = this.scrollY;
}

